require 'spec_helper'

RSpec.describe Sidekiq::RedisRouter::Router do
  def group(group, data = nil)
    Sidekiq::RedisRouter::Router::MatchedGroup.new(group, data)
  end

  def wrap_route(id, config)
    Sidekiq::RedisRouter::Router::Route.new(id, config)
  end

  let(:ins_management) { wrap_route("ins_management", double(:ins_management)) }
  let(:ins_backlog) { wrap_route("ins_backlog", double(:ins_backlog)) }
  let(:ins_cron_job) { wrap_route("ins_cron_job", double(:ins_cron_job)) }

  let(:ins_catchall) { wrap_route("ins_catchall", double(:ins_catchall)) }
  let(:ins_memory_bound) { wrap_route("ins_memory_bound", double(:ins_memory_bound)) }
  let(:ins_cpu_bound) { wrap_route("ins_cpu_bound", double(:ins_cpu_bound)) }

  let(:router) { described_class.new }

  before do
    router.config_instances(
      "ins_management" => ins_management.config,
      "ins_backlog" => ins_backlog.config,
      "ins_cron_job" => ins_cron_job.config,
      "ins_catchall" => ins_catchall.config,
      "ins_memory_bound" => ins_memory_bound.config,
      "ins_cpu_bound" => ins_cpu_bound.config
    )
    router.config_routes(
      management: "ins_management",
      backlog: "ins_backlog",
      cron_job: "ins_cron_job",
      queues: {
        default: "ins_catchall",
        mailer: "ins_catchall",
        memory_bound: "ins_memory_bound",
        cpu_bound: "ins_cpu_bound"
      }
    )
    router.config_groups([
      ["locks", :management],
      [/^worker:[^:]+:running$/, :management],
      [/^queue_locks:(?<queue>.*)$/, :queues]
    ])
  end

  # rubocop:disable Metrics/BlockLength
  where(:key, :matched_group, :route) do
    [
      ["schedule", group(:backlog), ref(:ins_backlog)],
      ["retry", group(:backlog), ref(:ins_backlog)],
      ["dead", group(:backlog), ref(:ins_backlog)],
      ["interrupted", group(:backlog), ref(:ins_backlog)],

      ["schedule_blah_blah", nil, nil],

      ["queue:default", group(:queues, "default"), ref(:ins_catchall)],
      ["queue:mailer", group(:queues, "mailer"), ref(:ins_catchall)],
      ["queue:memory_bound", group(:queues, "memory_bound"), ref(:ins_memory_bound)],
      ["queue:cpu_bound", group(:queues, "cpu_bound"), ref(:ins_cpu_bound)],
      ["queue:throttle", group(:queues, "throttle"), nil],

      [
        "working:queue:default:Nguyens-MacBook-Pro.local:78469:fe35a23a0340",
        group(:queues, "default"),
        ref(:ins_catchall)
      ],
      [
        "working:queue:cpu_bound:Nguyens-MacBook-Pro.local:78469:fe35a23a0340",
        group(:queues, "cpu_bound"),
        ref(:ins_cpu_bound)
      ],

      ["queues", group(:management), ref(:ins_management)],
      ["queues_blah_blah", nil, nil],

      ["stat:processed", group(:management), ref(:ins_management)],
      ["stat:processed:2021-11-18", group(:management), ref(:ins_management)],
      ["stat:failed", group(:management), ref(:ins_management)],
      ["stat:failed:2021-11-18", group(:management), ref(:ins_management)],
      ["processes", group(:management), ref(:ins_management)],
      ["Nguyens-MacBook-Pro.local:78469:fe35a23a0340", group(:management), ref(:ins_management)],
      ["Nguyens-MacBook-Pro.local:78469:fe35a23a0340-signals", group(:management), ref(:ins_management)],
      ["Nguyens-MacBook-Pro.local:78469:fe35a23a0340:workers", group(:management), ref(:ins_management)],

      ["cron_jobs", group(:cron_job), ref(:ins_cron_job)],
      ["cron_job:stuck_ci_jobs_worker", group(:cron_job), ref(:ins_cron_job)],
      ["cron_job:stuck_ci_jobs_worker:enqueued", group(:cron_job), ref(:ins_cron_job)],
      ["cron_job:stuck_ci_jobs_worker:jid_history", group(:cron_job), ref(:ins_cron_job)],

      ["locks", group(:management), ref(:ins_management)],
      ["worker:web_hook_worker:running", group(:management), ref(:ins_management)],
      ["queue_locks:cpu_bound", group(:queues, "cpu_bound"), ref(:ins_cpu_bound)]
    ]
  end
  # rubocop:enable Metrics/BlockLength

  with_them do
    it "matches correct group" do
      expect(router.find_group(key)).to eql(matched_group)
    end

    it "matches correct route" do
      expect(router.find_route(key)).to eql(route)
    end
  end
end
