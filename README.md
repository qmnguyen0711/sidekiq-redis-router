<img src="./sidekiq-redis-router-logo.png" width="200">

# Sidekiq::RedisRouter

This project is a gem that helps to route the workload of Sidekiq's Redis data
store to multiple Redis instances, one for main fallback instance and others for
each function group.

The routing is achieved by intercepting low-level Redis commands issued by
Sidekiq. The whole key space is seperated into different functional group. Each
functional group is a collection of relative keys, performing a particular
functionality inside Sidekiq.

One example is `management` group. It consists of:
- The collection of `queues`
- Processes and workers, such as `processes`, `{pid}:workers`,
`{identity}-signals`, etc.
- Sidekiq operational statistics, such as `stats:processed`,
`stats:processed:*`, `stats:failed`, etc.
- And other miscellaneous keys

Apart from built-in functional groups, each queue also forms a group. In a very
basic setting, queue-based groups only include `queue:queue_name` keys. The
detail of how each key is routed can be found [here]().

This gem wraps Sidekiq's redis connection pool with a proxy pool. This pool
returns a proxied connection. The connection intercepts a command for involved
group(s), and redirect that command to the correct Redis pool based on a
user-input mapping. Since this gem works at that level, it can redirect the
commands from both bultin commands, as well as user-defined components such as
sidekiq-cron, or Gitlab's Reliable Fetcher.

The implementation is optimized to reduce connection checking in and out from a
pool. This gem also supports `pipelined` to multiple instances fairly well. Of
course, it's quite obvious that any command involving keys resided in different
instances is can not be executed. This restriction also applies for independent
commands within a `multi` block.

By spliting the key spaces into relevant groups, this gem is able to honor the
atomicity of commands in most of the cases. There are some exceptions worth
noting about. One following section will mention each case and the reasons
behind.

Internally, this gem uses official Redis client. It means it is capable of
working with both Redis Sentinials and Redis Cluster. Unfortunately, Redis
Cluster does not seem to yield any certain benefits. Read more [at Sidekiq wiki
page](https://github.com/mperham/sidekiq/wiki/Using-Redis#architecture).

## Sidekiq Topology

As already mentioned above, the Router strategy comes with a key
disadvantage: cross-instance commands are prohibited. This should not be a big
problem for other applications. In Sidekiq case, the core functionality involves
two operations: push a job to a queue, and pull a job from a queue. The pushing
one is simple. The result command is `lpush queue:queue_a job_content`. Nothing
to worry here. The pulling operation is more interesting.

Sidekiq fetcher uses `brpop` command while GitLab Reliable Fetcher uses
`brpoplpush`, which works the same way. Both commands pull out the first element
from multiple keys in a blocking fashion. One Sidekiq queue is stored in one key
in Redis. As a result, this gem could not support the same where Sidekiq pulls
from queues located in multiple instances at once.

At GitLab, the Sidekiq fleet is sharded into many shards. Each shard is
responsible for a particular type of workload, such as `urgent_cpu_bound`,
`memory_bound`, `default`, to name a few. Redis processes of each shard listen
to a single queue, or a couple of queues in case of catchall shard. This gem
fits perfectly to this model.

```mermaid
flowchart TD
    subgraph Ruby Processes
    Clients[Sidekiq Clients]
    APIs[Sidekiq APIs]
    Router[Sidekiq::RedisRouter]
    end

    subgraph Redis Fleet
    RedisMain[Redis main]
    RedisA[Redis Instance A]
    RedisB[Redis Instance B]
    end

    subgraph Sidekiq Fleet
    ServersA[Sidekiq Servers of Shard A]
    ServersB[Sidekiq Servers of Shard B]
    Router2[Sidekiq::RedisRouter]
    end

    Clients-->Router
    APIs-->Router

    Router-- sadd queues default -->RedisMain
    Router-- zadd schedule 123 job3 -->RedisMain
    Router-- get stat:processed -->RedisMain
    Router-- lpush queue:a job1 -->RedisA
    Router-- lpush queue:b job2 -->RedisB

    ServersA-->Router2
    ServersB-->Router2
    Router2--brpop queue:a-->RedisA
    Router2--brpop queue:b-->RedisB
```

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'sidekiq-redis-Router'
```

Add the following lines to Sidekiq initializer:

```ruby
require 'sidekiq/redis-router'

# Configure fallback main Redis instance
Sidekiq.redis = { url: 'redis://localhost:3001' }
# Configure decomposed instances
Sidekiq::RedisRouter.instances(
    'instance_cpu_bound' => { url: 'redis://localhost:3002', db: 1 }
    'instance_memory_bound' => { url: 'redis://localhost:3003', namespace: 'abc' }
)

# Configure the router. Commands to groups not mentioned in the following
# configuration fallback to main instance.
Sidekiq::RedisRouter.route(
    queues: {
        urgent_cpu_bound: 'instance_cpu_bound'
        # Any config params that Sidekiq accepts
        memory_bound: 'instance_memory_bound'
    },
)
```

Most of the time, the internal function groups should stay in main. However, in
case we want to isolate them, we can always declare like following snipet. One
example is `schedule`, which acts as a temporary queue to store all jobs
triggered by `perform_in`. That queue is a hot key, making up of a significant
workload.

```ruby
Sidekiq::RedisRouter.route(
    management: 'instance_management',
    # Include "schedule", "retry", "dead"
    backlog: 'instance_backlog',
    # sidekiq-cron extention
    cron_job: 'instance_cron_jobs'
)
```

At GitLab, we built a lot of components on top of Sidekiq. Those components
interact directly with Sidekiq, touch existing keys, or even introduce new keys.
This gem also supports custom routing logic for such use cases.

```ruby
# Of course, we will need to route mentioned group to the corresponding instance afterward
Sidekiq::RedisRouter.group([
    # Introduced by GitLab Reliable Fetcher:
    # https://gitlab.com/gitlab-org/sidekiq-reliable-fetch/-/blob/97da2f6f405ea7cfcebe7efea3eb6c6d5aedfec1/lib/sidekiq/interrupted_set.rb
    ['interupted', :management],
    # Introduced by GitLab Reliable Fetcher. Those keys backs up the jobs
    # temporarily to restore when things go wrong. The first match is the queue
    # name.
    [/^.*working:queue:([^:]+)$/, :queues]
])
```