# frozen_string_literal: true

require "sidekiq"

require_relative "redis_router/version"
require_relative "redis_router/pool_proxy"
require_relative "redis_router/batch"
require_relative "redis_router/router"

module Sidekiq
  module RedisRouter
    ENABLEMENT_MUTEX = Mutex.new

    def self.enable!
      return if enabled?

      ENABLEMENT_MUTEX.synchronize do
        ::Sidekiq::RedisConnection.singleton_class.prepend ::Sidekiq::RedisRouter::Batch
        logger.info("Sidekiq::RedisRouter has just been enabled!")

        @enabled = true
      end
    end

    def self.enabled?
      ENABLEMENT_MUTEX.synchronize do
        @enabled == true
      end
    end

    def self.logger
      @logger ||= ::Logger.new(IO::NULL)
    end

    def self.logger=(logger)
      @logger = logger
    end

    def self.router
      @router ||= ::Sidekiq::RedisRouter::Router.new
    end

    def self.config_routes(routes)
      router.config_routes(routes)
    end

    def self.config_instances(instances)
      router.config_instances(instances)
    end
  end
end
