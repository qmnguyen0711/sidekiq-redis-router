# frozen_string_literal: true

module Sidekiq
  module RedisRouter
    class Router
      class InvalidMatcher < StandardError; end
      class NotSupportedGroup < StandardError; end

      MatchedGroup = Struct.new(:group, :data)
      Route = Struct.new(:id, :config)

      SUPPORTED_GROUP = %i[management backlog cron_job queues].freeze

      QUEUE_NAMED_CAPTURE = "queue"

      # The full details of how Sidekiq interacts with each key are available at
      # https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/1405
      DEFAULT_MATCH_RULES = [
        ["schedule", :backlog],
        ["retry", :backlog],
        ["dead", :backlog],
        # GitLab Reliable Fetcher's Interupted Set
        # https://gitlab.com/gitlab-org/sidekiq-reliable-fetch/-/blob/master/lib/sidekiq/interrupted_set.rb
        ["interrupted", :backlog],

        [/^queue:(?<queue>.*)$/, :queues],
        # GitLab Reliable Fetcher
        # https://gitlab.com/gitlab-org/sidekiq-reliable-fetch/-/blob/master/lib/sidekiq/base_reliable_fetch.rb
        [/^working:queue:(?<queue>[^:]+):[^:]*:\d+:[\w\d]+$/, :queues],

        ["queues", :management],
        ["stat:processed", :management],
        ["stat:failed", :management],
        [/^stat:processed:.*$/, :management],
        [/^stat:failed:.*$/, :management],
        ["processes", :management],
        # Worker Identity
        # https://github.com/mperham/sidekiq/blob/00ae6a74234a9b626a593ae6c5620416349f8eaf/lib/sidekiq/util.rb
        [/^[^:]*:\d+:[\w\d]+$/, :management],
        [/^[^:]*:\d+:[\w\d]+-signals$/, :management],
        [/^[^:]*:\d+:[\w\d]+:workers$/, :management],

        ["cron_jobs", :cron_job],
        [/^cron_job:[^:]+(:.+)?$/, :cron_job]
      ].freeze

      def initialize
        @instances = {}

        @functional_routes = {}
        @queue_routes = {}

        @string_group_matchers = {}
        @regexp_group_matchers = []

        config_groups(DEFAULT_MATCH_RULES)
      end

      def config_instances(instance_configs)
        instance_configs.each do |id, config|
          @instances[id] = config
        end
      end

      def config_routes(route_configs)
        route_configs = route_configs.dup

        queue_rule_configs = (route_configs.delete(:queues) || {})
        queue_rule_configs.each do |queue, instance_id|
          @queue_routes[queue.to_s] = instance_id.to_s
        end

        route_configs.each do |group, instance_id|
          group = group.to_sym
          validate_group!(group)

          @functional_routes[group] = instance_id.to_s
        end
      end

      def config_groups(rules)
        rules.each do |(rule, group)|
          group = group.to_sym
          validate_group!(group)
          validate_regexp_rule!(group, rule)

          case rule
          when String, Symbol
            @string_group_matchers[rule.to_s] = group
          when Regexp
            @regexp_group_matchers << [rule, group]
          else
            raise InvalidMatcher, "Invalid matcher type. Expected String, Symbol, or Regexp. Got #{rule.class}"
          end
        end
      end

      def find_group(key)
        return MatchedGroup.new(@string_group_matchers[key]) if @string_group_matchers[key]

        @regexp_group_matchers.each do |(matcher, group)|
          match = matcher.match(key)
          next if match.nil?

          if group == :queues
            return MatchedGroup.new(group, match.named_captures[QUEUE_NAMED_CAPTURE])
          else
            return MatchedGroup.new(group)
          end
        end

        nil
      end

      def find_route(key)
        matched_group = find_group(key)
        return if matched_group.nil?

        if matched_group.group == :queues
          wrap_route(@queue_routes[matched_group.data.to_s])
        else
          wrap_route(@functional_routes[matched_group.group.to_sym])
        end
      end

      private

      def validate_group!(group)
        raise NotSupportedGroup, "Group `#{group}` is not supported" unless SUPPORTED_GROUP.include?(group)

        raise InvalidMatcher, "Matcher for queue must include " unless SUPPORTED_GROUP.include?(group)
      end

      def validate_regexp_rule!(group, rule)
        return if group != :queues

        raise InvalidMatcher, "Matcher for queue must be a regexp" unless rule.is_a?(Regexp)

        unless rule.named_captures.key?(QUEUE_NAMED_CAPTURE)
          raise InvalidMatcher, "Matcher for queue must include a named capture called `queue`"
        end
      end

      def wrap_route(id)
        return if id.nil?

        instance_config = @instances[id]
        return if instance_config.nil?

        Route.new(id, instance_config)
      end
    end
  end
end
