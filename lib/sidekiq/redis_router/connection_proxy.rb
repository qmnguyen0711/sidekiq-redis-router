# frozen_string_literal: true

require 'redis-namespace'

module Sidekiq
  module RedisRouter
    class ConnectionProxy
      attr_reader :selected_pool

      def initialize(pool_proxy)
        @pool_proxy = pool_proxy
        @selected_pool = nil
        @pipelined = false
        @pipelined_results = []
      end

      def pipelined
        @pipelined = true
        yield
        @pipelined_results.clone
      ensure
        @pipelined = false
        @pipelined_results = []
      end

      def multi(&blk)
        main_pool.with do |conn|
          conn.multi do |multi|
            blk.call multi
          end
        end
      end

      DIRECT_COMMANDS = [
        Redis::Namespace::ADMINISTRATIVE_COMMANDS,
        Redis::Namespace::HELPER_COMMANDS
      ].compact.reduce(&:merge)
      PROXIED_COMMANDS = Redis::Namespace::NAMESPACED_COMMANDS
      ALL_COMMANDS = Redis::Namespace::COMMANDS

      PROXIED_COMMANDS.each_key do |command|
        next if method_defined?(command)

        define_method(command) do |*args, &block|
          call_with_proxy(command, *args, &block)
        end
      end

      DIRECT_COMMANDS.each_key do |command|
        next if method_defined?(command)

        define_method(command) do |*args, &block|
          main_pool.with do |conn|
            if conn.respond_to?(:namespace)
              conn.redis.send(command, *args, &block)
            else
              conn.send(command, *args, &block)
            end
          end
        end
      end

      def method_missing(command, *args, &block)
        main_pool.with do |conn|
          conn.send(command, *args, &block)
        end
      end

      def respond_to_missing?(command, *_args)
        ALL_COMMANDS.key?(command.to_s)
      end

      def call_with_proxy(command, *args, &block)
        handling = PROXIED_COMMANDS[command.to_s.downcase]

        (before,) = handling

        keys = []
        case before
        when :first
          keys << args[0] if args[0]
        when :all
          keys += args
        else
          # TODO: Handle other key positions
        end

        conn = select_pool(keys)
        # Dispatch the command to Redis and store the result.
        result = conn.send(command, *args, &block)
        @pipelined_results << result if @pipelined
        result
      end

      def clean_up
        @selected_pool&.checkin
      end

      private

      def main_pool
        @pool_proxy.main_pool
      end

      def select_pool(keys)
        pool = @pool_proxy.select_pool(keys)

        @selected_pool.checkin if !@selected_pool.nil? && @selected_pool != pool

        @selected_pool = pool
        @selected_pool.checkout
      end
    end
  end
end