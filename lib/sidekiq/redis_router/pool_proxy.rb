# frozen_string_literal: true

require_relative './connection_proxy'

module Sidekiq
  module RedisRouter
    class PoolProxy
      class CommandTargetMultipleInstances < StandardError; end

      def initialize(main_config, router, logger: Sidekiq::RedisRouter.logger, &pool_initializer)
        @main_config = main_config
        @router = router
        @pools = {}
        @pool_initializer = pool_initializer

        @logger = logger
      end

      def with
        conn = ::Sidekiq::RedisRouter::ConnectionProxy.new(self)
        result = yield conn
        conn.clean_up
        result
      end

      def size
        main_pool.size
      end

      def main_pool
        @main_pool ||= @pool_initializer.call(@main_config)
      end

      def select_pool(keys)
        return main_pool if keys.nil? || keys.empty?

        routes = keys.map { |key| @router.find_route(key) }.uniq

        if routes.size > 1
          raise CommandTargetMultipleInstances,
                "Command targets multiple Redis instances: #{routes.map(&:id)}. Involved keys: #{keys}"
        end

        route = routes.first

        if route.nil?
          @logger.debug("Routing keys #{keys}. Selected main pool")
          main_pool
        else
          @logger.debug("Routing keys #{keys}. Selected pool `#{route.id}`")
          find_pool(route)
        end
      end

      private

      def find_pool(route)
        @pools[route.id] ||= @pool_initializer.call(route.config)
      end
    end
  end
end