# frozen_string_literal: true

module Sidekiq
  module RedisRouter
    module Batch
      def create(main_config = {})
        ::Sidekiq::RedisRouter::PoolProxy.new(main_config, ::Sidekiq::RedisRouter.router) do |opts|
          super(opts)
        end
      end
    end
  end
end