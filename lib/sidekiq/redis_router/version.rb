# frozen_string_literal: true

module Sidekiq
  module RedisRouter
    VERSION = '0.1.0'
  end
end