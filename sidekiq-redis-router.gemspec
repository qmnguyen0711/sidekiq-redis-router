# frozen_string_literal: true

require_relative "lib/sidekiq/redis_router/version"

Gem::Specification.new do |spec|
  spec.name          = "sidekiq-redis-router"
  spec.version       = Sidekiq::RedisRouter::VERSION
  spec.authors       = ["Quang-Minh Nguyen"]
  spec.email         = ["qmnguyen@gitlab.com"]

  spec.summary       = "This project is a gem that helps decompose the workload of Sidekiq's Redis data store into multiple Redis instances from a set of configured rules"
  spec.homepage      = "https://gitlab.com/qmnguyen0711/sidekiq-redis-router"
  spec.required_ruby_version = Gem::Requirement.new(">= 2.7.0")

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/qmnguyen0711/sidekiq-redis-router"
  spec.metadata["changelog_uri"] = "https://gitlab.com/qmnguyen0711/sidekiq-redis-router/-/blob/main/CHANGELOG.md"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{\A(?:test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  # Uncomment to register a new dependency of your gem
  spec.add_dependency "redis-namespace", ">= 1.6.0"
  spec.add_dependency "sidekiq", "~> 6.4"
end
